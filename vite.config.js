import { resolve } from 'path';
import { defineConfig } from 'vite';
console.log('set!')
export default defineConfig({
  dev: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
      },
    },
  },
  server: {
        headers : {
            'Cross-Origin-Opener-Policy' : 'same-origin',
            'Cross-Origin-Embedder-Policy' : 'require-corp'
  },
}})
